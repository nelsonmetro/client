import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#757575', // darken1
        secondary: '#F5F5F5', // lighten4
        accent: '#9E9E9E' // base 
      }
    }
  }
});
