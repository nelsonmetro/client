import HTTP from '../../http';
import { Remarkable } from 'remarkable';

const state = () => ({
  questions: [],
  replies: [],
  response: { sm: 4, md: 4 },
  section: null
});

// actions
const actions = {
  entrance({ commit, state }) {
    return HTTP()
      .get(`/quest/${state.section}`)
      .then(({ data }) => {
        var list = [];
        var quest = '';
        var element = -1;
        var md = new Remarkable({ html: true });

        data.map((currVal) => {
          if (quest !== currVal.Q) {
            quest = currVal.Q;
            element++;

            list[element] = {
              id: currVal.Q,
              type: currVal.TYPE,
              t: currVal.T,
              synopsis:
                '<p style="font-size: 10pt;margin-bottom: 4px;"' + md.render(currVal.TALE).substr(2, md.render(currVal.TALE).length - 2),
              question: md.render(currVal.QUESTION),
              info: currVal.INFO,
              answer: []
            };
          }

          list[element].answer.push({
            id: currVal.A,
            val: currVal.ANSWER
          });
        }); 
        commit('loadQuest', list);

        list.forEach((question, i) => {
          switch (question.t) {
            case 'CB':
              commit('multiSelect', { question: question, i: i });
              break;
            case 'RB':
              commit('singleSelect', { question: question, i: i });
              break;
            case 'LP':
              commit('lpcScale', { question: question, i: i });
              break;
            case 'LS':
              commit('linkScale', { question: question, i: i });
              break;
          }
        });
      })
      .catch((ex) => {
        console.error(ex);
      });
  }
};

const mutations = {
  loadsection(state, section) {
    state.section = section;
  },
  loadQuest(state, quest) {
    state.questions = quest;
  },
  multiSelect(state, obj) {
    const LENGTH = obj.question.answer.length;
    var col = [];
    var row = [];
    var dim = { x: 2, y: 3 };

    switch (LENGTH) {
      case 2:
        state.response.sm = 6;
        state.response.md = 6;
        dim.x = 1;
        dim.y = 2;
        break;
    }

    for (let x = 0; x < LENGTH; x += dim.x) {
      let ansr = obj.question.answer.slice(x, x + dim.x);
      col.push(ansr);
    }
    for (let y = 0; y < col.length; y += dim.y) {
      let ansr = col.slice(y, y + dim.y);
      row.push(ansr);
    }
    state.questions[obj.i].answer = row;
  },
  singleSelect(state, obj) {
    const LENGTH = obj.question.answer.length;
    var ref = [...obj.question.answer];
    const options = [[], [], []];

    for (let x = 0, i = 0; i < LENGTH; x++, i++) {
      if (x == options.length) x = 0;
      options[x].push(ref[i]);
    }
    switch (LENGTH) {
      case 2:
        state.response.sm = 6;
        state.response.md = 6;
        break;
      default:
        state.response.sm = 6;
        break;
    }

    state.questions[obj.i].answer = options;
  },
  lpcScale(state, obj) {
    const LENGTH = obj.question.answer.length;
    var ref = [...obj.question.answer];
    var values = ref.splice(1, LENGTH - 2);

    state.questions[obj.i].answer = {
      left: { label: ref[0].val, val: values[0].id },
      range: values.map((i) => {
        return i.val;
      }),
      right: { label: ref[1].val, val: values[LENGTH - 3].id }
    };
  },
  linkScale(state, obj) {
    state.questions[obj.i].answer = obj.question.answer;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
