export default {
  namespaced: true,
  state: {
    drawer: null
  },
  actions: {
    draw({ commit, state }) {
    }
  },
  mutations: {
    drawerToggle(state, update) {
      state.drawer = update;
    }
  }
};

