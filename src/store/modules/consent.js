import HTTP from '../../http';

export default {
  namespaced: true,
  state: {
    title: null,
    fname: null,
    lname: null,
    email: null,
    gps: null,
    voluntary: true,
    noconsequence: true,
    language: true,
    nocost: true
  },
  actions: {
    processform({ commit, state }) {
      const headers = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      };
      const arg = {
        fname: state.fname,
        lname: state.lname,
        title: state.title,
        email: state.email,
        volun: state.voluntary,
        nocon: state.noconsequence,
        langu: state.language,
        nocos: state.nocost,
        locat: state.gps
      };
      return (
        HTTP()
          .post('/participant/add', arg, headers)
          .then(({ data }) => {
            console.log(data);
          })
          .catch((ex) => {
            console.error(ex);
          })
      );
    }
  },
  mutations: {
    fnameInput(state, fname) {
      state.fname = fname;
    },
    lnameInput(state, lname) {
      state.lname = lname;
    },
    titleInput(state, title) {
      state.title = title;
    },
    emailInput(state, email) {
      state.email = email;
    },
    consentbox1(state, voluntary) {
      state.voluntary = !!voluntary;
    },
    consentbox2(state, noconsequence) {
      state.noconsequence = !!noconsequence;
    },
    consentbox3(state, language) {
      state.language = !!language;
    },
    consentbox4(state, nocost) {
      state.nocost = !!nocost;
    },
    locationInput(state, gps) {
      state.gps = gps;
    },
    updatelocation(state, gps) {
      state.gps = gps;
    }
  }
};
