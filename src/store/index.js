// import createPersistedState from 'vuex-persistedstate';
import Vue from 'vue';
import Vuex from 'vuex';
import consent from './modules/consent';
import quest from './modules/quest';
import gui from './modules/gui';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  state: {
    baseUrl: '/api'
  },
  mutations: {},
  actions: {},
  // plugins: [createPersistedState()],
  modules: {
    consent,
    quest,
    gui
  }
});
