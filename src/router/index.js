import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import About from '../views/About.vue';
import Terms from '../views/Terms.vue';
import Quest1 from '../views/Quest1.vue';
import Quest2 from '../views/Quest2.vue';
import Quest3 from '../views/Quest3.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/terms',
    name: 'Tems',
    component: Terms
  },
  {
    path: '/quest1',
    name: 'Quest1',
    component: Quest1
  },
  {
    path: '/quest2',
    name: 'Quest2',
    component: Quest2
  },
  {
    path: '/quest3',
    name: 'Quest3',
    component: Quest3
  }
];

const router = new VueRouter({
  routes
});

export default router;
